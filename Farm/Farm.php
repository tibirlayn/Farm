<?php
///**
// * undocumented class
// *
// * @package default
// * @author `Artur Zaharov`
// */

interface Animal {

}

interface CanGiveMilk {
    public function getMilk(): int;
}

interface CanGiveEggs {
    public function getEggs(): int;
}

interface Storage { //хранилище молока, яиц

    public function addMilk(int $liters);

    public function addEggs(int $eggsCount);

    public function getFreeSpaceForMilk(): int;

    public function getFreeSpaceForEggs(): int;

    public function howMuchMilk(): int;

    public function howMuchEggs(): int;

}

class Cow implements Animal, CanGiveMilk {  // корова
    public static $sumAnimalCowStatic = 0;
    public $id;

    public function __construct()
    {
        $this->id = md5(rand()); //случайное id
    }

    public function getMilk(): int
    {
        return rand(8, 12); //выдает 8-12 литров молока
    }
}

class Chicken implements Animal, CanGiveEggs { // курица
    public static $sumAnimalChickenStatic = 0;
    public $id;

    public function __construct()
    {
        $this->id = md5(rand()); //случайное id
    }

    public function getEggs(): int
    {
        return rand(0, 1); //выдает 0-1 яичек
    }
}

class Barn implements Storage { //амбар

    private $milkLiters = 0;
    private $eggsCount = 0;
    private $milkLimit = 0;
    private $eggsLimit = 0;

    public function __construct(int $milkLimit, int $eggsLimit)
    {
        $this->milkLimit = $milkLimit; //вместимость по молоку
        $this->eggsLimit = $eggsLimit; //вместимость по яйцам
    }

    public function addMilk(int $liters) // добавить молоко
    {
        $freeSpace = $this->getFreeSpaceForMilk();

        if ($freeSpace === 0) { //абмар заполнен, места нет
            return;
        }

        if ($freeSpace < $liters) { // если амбарт заполнен, тогда заполняем его до конца
            $this->milkLiters = $this->milkLimit;
            return;
        }

        $this->milkLiters += $liters; // заполняем амбарт
    }

    public function addEggs(int $eggsCount) // добавить яиц
    {
        $freeSpace = $this->getFreeSpaceForEggs();

        if ($freeSpace === 0) {
            return;
        }

        if ($freeSpace < $eggsCount) { // если амбарт заполнен, тогда заполняем его до конца
            $this->eggsCount = $this->eggsLimit;
            return;
        }

        $this->eggsCount += $eggsCount; // заполняем амбарт
    }

    public function getFreeSpaceForMilk(): int //считаем свободное место молоко
    {
        return $this->milkLimit - $this->milkLiters;
    }

    public function getFreeSpaceForEggs(): int //считаем свободное место яйца
    {
        return $this->eggsLimit - $this->eggsCount;
    }

    public function howMuchMilk(): int  // возращает кол-во молока
    {
        return $this->milkLiters;
    }

    public function howMuchEggs(): int // возращает кол-во яиц
    {
        return $this->eggsCount;
    }
}

class Farm { // Ферма

    private $name; // имя фермы
    private $storage;
    private $animals = []; // массив для животных

    public function __construct(string $name, Storage $storage)
    {
        $this->name = $name;
        $this->storage = $storage;
    }

    public function sumAnimalCow() { // возращаем кол-во животных коров
        return Cow::$sumAnimalCowStatic;
    }

    public function sumAnimalChicken() { // возращаем кол-во куриц
        return Chicken::$sumAnimalChickenStatic;
    }

    public function returnMilk() // возращаем кол-во молока
    {
        return $this->storage->howMuchMilk();

    }

    public function returnEggs() // возращаем кол-во яиц
    {
        return $this->storage->howMuchEggs();

    }

    public function addAnimal(Animal $animal)
    {
        $this->animals[] = $animal; //добавляем животное в массив
    }

    public function collectProducts() //сбор продукции
    {
        foreach ($this->animals as $animal) {
            if ($animal instanceof CanGiveMilk) { //если относится new Cow()
                $milkLiters = $animal->getMilk(); //получает рандомное число от rand(8, 12)
                $this->storage->addMilk($milkLiters);
                Cow::$sumAnimalCowStatic++;
            }

            if ($animal instanceof CanGiveEggs) { //если относится new Chicken()
                $eggsCount = $animal->getEggs(); //получает рандомное число от rand(0, 1)
                $this->storage->addEggs($eggsCount);
                Chicken::$sumAnimalChickenStatic++;
            }
        }
    }
}

$barn = new Barn($milkLimit = 200, $eggsLimit = 200); // кол-во свободного мест для молока и яиц
$fastFarm = new Farm('FastFarm', $barn); // название фермы и амбар

for ($i = 0; $i < 10; $i++) { // добавляем коров
    $fastFarm->addAnimal(new Cow());
}

for ($i = 0; $i < 20; $i++) { // добовляем куриц
    $fastFarm->addAnimal(new Chicken());
}

$fastFarm->collectProducts(); //собираем продукты
echo "Кол-во коров = ".$fastFarm->sumAnimalCow()."\n";
echo "Кол-во молока = ".$fastFarm->returnMilk()."\n"; //вывод результата
echo "-----------------------------------------------\n";
echo "Кол-во куриц = ".$fastFarm->sumAnimalChicken()."\n";
echo "Кол-во яиц = ".$fastFarm->returnEggs()."\n"; //вывод результата